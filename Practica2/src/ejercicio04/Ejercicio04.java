package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
		
		String cadena = null;
		int num;
		int suma = 0;
		int cadenaInt; 
		do {
			System.out.println("Introduzca un numero, despues si quieres parar, introduce fin");
			cadena = lector.nextLine();
			cadenaInt = Integer.parseInt(cadena);
			System.out.println(cadenaInt);
			lector.next();
			num = lector.nextInt();
			suma += num;
		}while(cadena != "fin");
		System.out.println("La suma de todos los numeros es " + suma);
		lector.close();

	}

}
