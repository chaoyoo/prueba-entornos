package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
		
		System.out.println("Introduce una cadena");
		String finale = lector.nextLine();
		String cadena;
		int coincidencia;
		int numCadenas5 = 0;
		do {
			System.out.println("Introduce la cadena anterior");
			cadena=lector.nextLine();
			cadena=cadena.toLowerCase();
			coincidencia=cadena.compareTo(finale);
			if(cadena.length()>4) {
				numCadenas5++;
			}
		}while(coincidencia!=0);
		if(numCadenas5>0) {
			System.out.println("Se ha introducido una cadena de 5 caracteres o m�s.");
		}
		lector.close();

	}

}
