package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
		int opcion;

		do {
			System.out.println("1 � contar may�sculas\r\n" + "2 � contrase�a correcta\r\n" + "3 - salir ");
			opcion = lector.nextInt();
			switch (opcion) {

			case 1:
				lector.nextLine();
				System.out.println("Dame una cadena");
				String cadena = lector.nextLine();
				System.out.println(cadena);

				break;

			case 2:
				lector.nextLine();
				System.out.println("Introduce una contrase�a");
				String contrase�a = lector.nextLine();
				contrase�a = contrase�a.toLowerCase();
				String vocal1 = "a";
				String vocal2 = "e";
				String vocal3 = "i";
				String vocal4 = "o";
				String vocal5 = "u";
				String num1 = "1";
				String num2 = "2";
				String num3 = "3";
				String num4 = "4";
				String num5 = "5";
				String num6 = "6";
				String num7 = "7";
				String num8 = "8";
				String num9 = "9";
				String num0 = "0";
				String barraBaja = "_";
				String espacio = " ";
				String arroba = "@";
				String barra = "-";
				String hashtag = "#";
				String igual = "=";
				String interrogacion1 = "�";
				String interrogacion2 = "?";
				String exclamacion1 = "�";
				String exclamacion2 = "!";
				String parentesis1 = "(";
				String parentesis2 = ")";
				String comilla = "'";
				String porc = "%";
				String punto = ".";
				String coma = ",";
				String and = "&";
				String barraLateral = "/";

				if (contrase�a.contains(vocal1) || contrase�a.contains(vocal2) || contrase�a.contains(vocal3)
						|| contrase�a.contains(vocal4) || contrase�a.contains(vocal5)) {
					if (contrase�a.contains(num0) || contrase�a.contains(num1) || contrase�a.contains(num2)
							|| contrase�a.contains(num3) || contrase�a.contains(num4) || contrase�a.contains(num5)
							|| contrase�a.contains(num6) || contrase�a.contains(num7) || contrase�a.contains(num8)
							|| contrase�a.contains(num9)) {
						System.out.println("No es v�lida, has utilizado numeros.");
					} else if (contrase�a.contains(barraBaja) || contrase�a.contains(barra)
							|| contrase�a.contains(espacio) || contrase�a.contains(hashtag)
							|| contrase�a.contains(arroba) || contrase�a.contains(interrogacion1)
							|| contrase�a.contains(interrogacion2) || contrase�a.contains(exclamacion1)
							|| contrase�a.contains(exclamacion2) || contrase�a.contains(igual)
							|| contrase�a.contains(parentesis1) || contrase�a.contains(parentesis2)
							|| contrase�a.contains(comilla) || contrase�a.contains(porc)
							|| contrase�a.contains(punto) || contrase�a.contains(coma)
							|| contrase�a.contains(and) || contrase�a.contains(barraLateral)) {
						System.out.println("No es v�lida, has utilizado caracteres especiales.");
					} else {
						System.out.println("La contrase�a es v�lida");
					}

				} else {
					System.out.println("No es v�lida, necesita tener al menos una vocal.");
				}

				break;

			case 3:
				System.out.println("Salir");
				break;

			default:
				System.out.println("Opci�n incorrecta");
				break;
			}
		} while (opcion != 3);
		lector.close();

	}

}
