package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
		System.out.println("Introduce un numero de veces");
		int veces = lector.nextInt();
		int num;
		int acumulador=0;
		for(int i=0; i<veces; i++) {
			System.out.println("Dame un numero");
			num = lector.nextInt();
			if(num%2==0) {
				System.out.println("Es par");
			} else {
				System.out.println("Es impar");
			}
			acumulador +=num;
		}
		System.out.println("La media de todos es " + (acumulador/veces));
		lector.close();

	}

}
